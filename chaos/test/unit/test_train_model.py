"""Unit tests of the functions in chaos/domain/train_model.py.
"""
import os
import pathlib
import pickle

import pytest
import pandas as pd
import numpy as np
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression

import chaos.settings.settings as stg

from chaos.domain.train_model import train_model, evaluate_model

customers_test = pd.DataFrame(data={
    'ID_CLIENT': ["15698028", "01234567","15678028", "01244567", "15678128","15698038", "012312567","156743028",
                  "012420567", "156700128"],
    'DATE_ENTREE': ["2019-02-01 00:00:00", "2018-04-01 00:00:00","2012-02-01 00:00:00", "2014-06-01 00:00:00",
                    "2014-04-01 00:00:00","2019-02-01 00:00:00", "2018-04-01 00:00:00","2012-02-01 00:00:00",
                    "2014-06-01 00:00:00","2014-04-01 00:00:00"],
    'NOM': ['Random', 'Guy', 'Other', 'Who', 'None','Random', 'Guy', 'Other', 'Who', 'None'],
    'PAYS': ['France', 'Allemagne', 'Espagne', 'France', 'France','France', 'Allemagne', 'Espagne', 'France', 'France'],
    'SEXE': ['F', 'H', 'H', 'F', 'F','F', 'H', 'H', 'F', 'F'], 'AGE': [41, 114, 34, 60, 70,41, 114, 34, 60, 70],
    'MEMBRE_ACTIF': ['No', 'No', 'Yes', 'No', 'Yes','No', 'No', 'Yes', 'No', 'Yes']
    })

indicators_test = pd.DataFrame(data={
    'ID_CLIENT': ["15698028", "01234567","15678028", "01244567", "15678128","15698038", "012312567","156743028", "012420567", "156700128"],
    'BALANCE': [0.0, 50.0, 5430.0, 29845.0, 0.0,0.0, 50.0, 5430.0, 29845.0, 0.0],
    'NB_PRODUITS': [2, 1, 1, 3, 2,2, 1, 1, 3, 2],
    'CARTE_CREDIT': ['Yes', 'Yes', 'No', 'Yes', 'No','Yes', 'Yes', 'No', 'Yes', 'No'],
    'SALAIRE': [31766.3, 21303.0, 120.0, 150652.0, 2100.0,31766.3, 21303.0, 120.0, 150652.0, 2100.0],
    'SCORE_CREDIT': [np.nan, 430.0, 655.0, 800.0, 350.0,np.nan, 430.0, 655.0, 800.0, 350.0],
    'CHURN': ["Yes", "No", 'No', 'No', 'Yes',"Yes", "No", 'No', 'No', 'Yes']
})

def test_train_model():
    indicators_test.to_csv('indicators.csv', index=False, sep=stg.INDICATORS_SEP)
    customers_test.to_csv('customers.csv', index=False, sep=stg.CUSTOMERS_SEP)

    data_model_dict = train_model('customers.csv', 'indicators.csv', 'model.pkl')

    os.remove('indicators.csv')
    os.remove('customers.csv')
    os.remove('model.pkl')

    model = data_model_dict['model']
    X = data_model_dict['X']
    y = data_model_dict['y']

    pipe = Pipeline(steps=[("log_reg", LogisticRegression())])
    assert type(model) == type(pipe)
    assert type(X) == type(indicators_test)
    assert y[0] in [0, 1]
    assert y[1] in [0, 1]

def test_evaluate_model():
    indicators_test.to_csv('indicators.csv', index=False, sep=stg.INDICATORS_SEP)
    customers_test.to_csv('customers.csv', index=False, sep=stg.CUSTOMERS_SEP)

    data_model_dict = evaluate_model('customers.csv', 'indicators.csv')

    os.remove('indicators.csv')
    os.remove('customers.csv')
    assert True