"""This module provides classes to clean and impute values on a dataframe already 
technically cleaned.

Classes
-------
FrequencyEncoder

CustomOneHotEncoder

AberrantAgeImputer

AberrantNbProduitsImputer

CreditScoreImputer

SalaryImputer

BalanceImputer
"""

import os
import sys
import datetime
import pandas as pd

from sklearn.base import BaseEstimator, TransformerMixin

import chaos.settings.settings as stg

class FrequencyEncoder(BaseEstimator, TransformerMixin):
    """Transform columns with frequency encoding.

    Attributes
    ----------
    columns: list
        List of columns to which perform a frequency encoding.
    frequence_dict: dict, optionnal
        Frequencies of distinct values.

    """

    def __init__(self, columns):
        """Initialize class with dictionnary whose keys are column names to transform.
        
        Arguments
        ---------
        columns: list
        """
        self.columns = columns
        self.frequency_dict = {key:None for key in columns}

    def fit(self, X, y=None):
        """Compute frequency of each value for columns in X.

        Arguments
        ---------
        X: pandas.DataFrame
            Any dataframe.
        
        Returns
        -------
        self: FrequencyEncoder
            Transformed FrequencyEncoder object.
        """
        X_ = X.copy()
        for col in self.columns:
            self.frequency_dict[col] = (X_.groupby(col).size()) / len(X_)
        return self

    def transform(self, X, y=None):
        """Transform values to numerical values based on their frequency in X.

        Arguments
        ---------
        X: pandas.DataFrame
            Any dataframe.
        
        Returns
        -------
        X_: pandas.DataFrame
            Transformed dataframe.
        """
        X_ = X.copy()
        for col in self.columns:
            X_[col] = X_[col].apply(lambda x: self.frequency_dict[col][x])
        return X_

class CustomOneHotEncoder(BaseEstimator, TransformerMixin):
    """Tranform columns using one hot encoding.
    
    Attributes
    ----------
    features_to_encode: list
        Columns to transform.

    """

    def __init__(self, columns):
        """Initialize class.
        
        Arguments
        ---------
        columns: list
        """
        self.columns = columns
        
    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """One hot encode features.
        
        Arguments
        ---------
        X: pandas.DataFrame
            Any dataframe.

        Returns
        -------
        X_: pandas.DataFrame
            Transformed dataframe.
        """
        X_ = X.copy()
        for column in self.columns:
            X_ = pd.get_dummies(data=X_, columns=[column], prefix=column, dtype='int64')
        return X_

class AberrantAgeImputer(BaseEstimator, TransformerMixin):
    """Transform ages over 100.

    """

    def __init__(self):
        """Initialize class."""
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Transform age column.
        
        Arguments
        ---------
        X: pandas.DataFrame
            Dataframe coming from a RawBankData object.
        
        Returns
        -------
        X_: pandas.DataFrame
            Transformed dataframe.
        """
        X_ = X.copy()
        X_[stg.AGE] = X_[stg.AGE].apply(lambda x: x % 100)
        return X_

class AberrantNbProduitsImputer(BaseEstimator, TransformerMixin):
    """Transform NB_PRODUITS over 10.

    """

    def __init__(self):
        """Initialize class."""
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Transform NB_PRODUITS column.
        
        Arguments
        ---------
        X: pandas.DataFrame
            Dataframe coming from a RawBankData object.
        
        Returns
        -------
        X_: pandas.DataFrame
            Transformed dataframe.
        """
        X_ = X.copy()
        X_[stg.NB_PRODUITS] = X_[stg.NB_PRODUITS].apply(lambda x: x % 14)
        return X_

class CreditScoreImputer(BaseEstimator, TransformerMixin):
    """Transform nan in a column.
    
    Arguments
    ---------
    value: float
        Value to input nan values in SCORE_CREDIT column.
    
    """

    def __init__(self, value):
        """Initialize class.
        
        Arguments
        ---------
        value: float
        """
        self.value = value

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Imput nan in SCORE_CREDIT column with fixed value.
        
        Arguments
        ---------
        X: pandas.DataFrame
            Dataframe coming from a RawBankData object.
        
        Returns
        -------
        X_: pandas.DataFrame
            Transformed dataframe.
        """
        X_ = X.copy()
        X_[stg.SCORE_CREDIT] = X_[stg.SCORE_CREDIT].fillna(value=self.value)
        return X_

class SalaryImputer(BaseEstimator, TransformerMixin):
    """Transform nan in a column.
    
    Arguments
    ---------
    value: float
        Value to input nan values in SALAIRE column.

    """

    def __init__(self, value):
        """Initialize class.
        
        Arguments
        ---------
        value: float
        """
        self.value = value
        

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        """Impute nan in SALAIRE column with fixed value.
        
        Arguments
        ---------
        X: pandas.DataFrame
            Dataframe coming from a RawBankData object.
        
        Returns
        -------
        X_: pandas.DataFrame
            Transformed dataframe.
        """
        X_ = X.copy()
        X_[stg.SALAIRE] = X_[stg.SALAIRE].fillna(value=self.value)
        return X_

class BalanceImputer(BaseEstimator, TransformerMixin):
    """Transform nan in a column.
    
    Arguments
    ---------
    median_balance: float
        Median of the BALANCE column.

    """

    def __init__(self):
        """Initialize class."""
        self.median_balance = None

    def fit(self, X, y=None):
        """Compute median of BALANCE column in X.
        
        Returns
        -------
        self: BalanceImputer
            Transformed BalanceImputer object.
        """
        X_ = X.copy()
        self.median_balance = X_[stg.BALANCE].median()
        return self

    def transform(self, X, y=None):
        """Impute nan with value of BALANCE median calculated in fit method.
        
        Arguments
        ---------
        X: pandas.DataFrame
            Dataframe coming from a RawBankData object.
        
        Returns
        -------
        X_: pandas.DataFrame
            Transformed dataframe.
        """
        X_ = X.copy()
        X_[stg.BALANCE] = X_[stg.BALANCE].fillna(value=self.median_balance)
        return X_
