"""This module provides functions to make churn predictions.

Functions
---------
predict_churn

import_model_bucket

"""
import pickle

import pandas as pd
import numpy as np

from google.cloud import storage, exceptions

import chaos.settings.settings as stg
import chaos.settings.user_settings as ustg

from chaos.infrastructure.bank_data import BankData

def predict_churn(customers_file, indicators_file, model_filename=stg.MODEL_FILENAME):
    """Create csv file with churn predictions.
    
    Arguments
    ---------
    customers_filepath: str or file-like object
        Media type should be csv.
    indicators_filepath: str or file-like object
        Media type should be csv.
    
    Returns
    -------
    X : pandas DataFrame
        DataFrame with features and predictions.
    """
    # Reset
    if type(customers_file) != str:
        customers_file.seek(0)
    if type(indicators_file) != str:
        indicators_file.seek(0)
    
    # Import Data
    bank_data = BankData(
        customers_file=customers_file,
        indicators_file=indicators_file,
        customers_sep=ustg.CUSTOMERS_TEST_SEPARATOR,
        indicators_sep=ustg.INDICATORS_TEST_SEPARATOR
    )
    X = bank_data.data

    # Import model
    try:
        model = import_model(model_filename)
    except FileNotFoundError as error:
        print("==> Model not found locally. Checking in remote bucket")
        print("==> Error type: ", error)
        model = import_model_bucket()

    # Predict
    y_proba = model.predict_proba(X)[:, 1]
    y_pred = (y_proba >= stg.THRESHOLD).astype(int)

    # Create output dataframe
    X[stg.CHURN_PREDICTION] = y_pred
    X[stg.CHURN_PREDICTION] = X[stg.CHURN_PREDICTION].map(stg.MAPPING[stg.CHURN_PREDICTION])
    if bank_data.dataframe_with_nan.empty == False:
        bank_data.dataframe_with_nan[stg.CHURN_PREDICTION] = np.nan
        X = pd.concat([X, bank_data.dataframe_with_nan], axis=0)

    return X

def import_model_bucket(bucket_name=stg.BUCKET_NAME, source_blob_name=stg.MODEL_GS_FILENAME):
    """Import pickle model already trained form bucket.
    
    Arguments
    ---------
    bucket_name: str
        Bucket name.
    source_blob_name: str
        Path to the file trained model in bucket.

    Return
    ------
    model: scikit-learn Pipeline
        Fitted pipeline.
    """
    try:
        # Google Storage Client
        storage_client = storage.Client()
        bucket = storage_client.bucket(bucket_name)
        blob = bucket.blob(source_blob_name)
        pickle_in = blob.download_as_bytes()
        # Model as pickle
        data_model_dict = pickle.loads(pickle_in)
        model = data_model_dict['model']
    
    except exceptions.NotFound as error:
            print("==> WARNING Model not found in bucket.")
            print("==> Error type: ", error)

    return model

def import_model(file_path=stg.MODEL_FILENAME):
    """Import pickle model already trained.
    
    Arguments
    ---------
    file_path: str
        Path to the trained model.

    Return
    ------
    model: scikit-learn Pipeline
        Fitted pipeline.
    """
    with open(file_path, 'rb') as handle:
        data_model_dict = pickle.load(handle)
        model = data_model_dict['model']
    
    return model
