from os import path
import chaos.settings.settings as stg

# Enter filenames :
CUSTOMERS_TEST_FILENAME = 'customers_test.csv'
INDICATORS_TEST_FILENAME = 'indicators_test.csv'
LABELS_TEST_FILENAME = 'y_test.csv'

# Enter separator for your files :
CUSTOMERS_TEST_SEPARATOR = ";"
INDICATORS_TEST_SEPARATOR = ";"

# As an indication :
CUSTOMERS_TRAIN_FILENAME = stg.CUSTOMERS_FILENAME
INDICATORS_TRAIN_FILENAME = stg.INDICATORS_FILENAME


CUSTOMERS_TEST_FILEPATH = path.abspath(path.join(stg.TEST_DATA_DIR, CUSTOMERS_TEST_FILENAME))
INDICATORS_TEST_FILEPATH = path.abspath(path.join(stg.TEST_DATA_DIR, INDICATORS_TEST_FILENAME))
JSON_TEST_FILEPATH = path.abspath(path.join(stg.TEST_DATA_DIR, 'dataframe_sample.json'))

