.. _technicalcleaningmodule: technicalcleaning

Module technical_cleaning
*************************
This document describes all classes of the module technical_cleaning.

.. automodule:: chaos.infrastructure.technical_cleaning

============================================================
Class
============================================================

.. autoclass:: chaos.infrastructure.technical_cleaning.DateTransformer()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.infrastructure.technical_cleaning.DateTransformer.transform()

============================================================
Class
============================================================

.. autoclass:: chaos.infrastructure.technical_cleaning.CategoricalTypeTransformer()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.infrastructure.technical_cleaning.CategoricalTypeTransformer.transform()

============================================================
Class
============================================================

.. autoclass:: chaos.infrastructure.technical_cleaning.BooleanEncoder()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.infrastructure.technical_cleaning.BooleanEncoder.transform()







    
