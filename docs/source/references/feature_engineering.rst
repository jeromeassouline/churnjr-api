.. _featureengineeringmodule: featureengineering

Module feature_engineering
**************************
This document describes all classes of the module feature_engineering.

.. automodule:: chaos.domain.feature_engineering

============================================================
Class
============================================================

.. autoclass:: chaos.domain.feature_engineering.SeniorityCreator()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.domain.feature_engineering.SeniorityCreator.transform()

============================================================
Class
============================================================

.. autoclass:: chaos.domain.feature_engineering.AgeClassCreator()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.domain.feature_engineering.AgeClassCreator.transform()

============================================================
Class
============================================================

.. autoclass:: chaos.domain.feature_engineering.CreditScoreAgeRatioCreator()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.domain.feature_engineering.CreditScoreAgeRatioCreator.transform()

============================================================
Class
============================================================

.. autoclass:: chaos.domain.feature_engineering.BalanceWageRatioCreator()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.domain.feature_engineering.BalanceWageRatioCreator.transform()

============================================================
Class
============================================================

.. autoclass:: chaos.domain.feature_engineering.CreditScoreByNProductsCreator()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.domain.feature_engineering.CreditScoreByNProductsCreator.transform()

============================================================
Class
============================================================

.. autoclass:: chaos.domain.feature_engineering.FeatureDropper()

************************************************************
    Methods
************************************************************

    .. automethod:: chaos.domain.feature_engineering.FeatureDropper.transform()

